import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "./registerServiceWorker";

Vue.config.productionTip = false;
Vue.use(Vuetify, {
  theme: {
    primary: "#019BE5",
    secondary: "#0254B7",
    accent: "#009BDE",
    error: "#FF5252",
    info: "#2196F3",
    success: "#00B23D",
    warning: "#D5C216",
    save: "#292aa8"
  },
  breakpoint: {
    thresholds: {
      xs: 360
    },
    scrollbarWidth: 10
  }
});
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
