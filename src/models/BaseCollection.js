import { Collection } from "vue-mc";

export default class BaseCollection extends Collection {
  getRequest(config) {
    config.baseURL = process.env.VUE_APP_API_BASE_URL;
    config.headers = { "Access-Control-Allow-Origin": "*" };
    return super.getRequest(config);
  }
}
