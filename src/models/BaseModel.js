import { Model } from "vue-mc";


export default class BaseModel extends Model {
  getRequest(config) {
    config.baseURL = process.env.VUE_APP_API_BASE_URL;
    //config.baseURL = config.baseURL = process.env.BASE_URL;
    //config.headers = { 'Authorization': 'Bearer ' + localStorage.getItem('provider_token'),};
    return super.getRequest(config);
  }
}
