import BaseCollection from "../BaseCollection";
import Category from "./Category";

export default class Categories extends BaseCollection {
  model() {
    return Category;
  }

  defaults() {
    return {
      orderBy: "name"
    };
  }

  routes() {
    return {
      fetch: "categories"
    };
  }
}
