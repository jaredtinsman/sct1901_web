import BaseModel from "../BaseModel";
import {
  boolean,
  equal,
  integer,
  min,
  required,
  string
} from "vue-mc/validation";
export default class Category extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
      description: null
    };
  }

  validation() {
    return {
      id: integer.and(min(1)).or(equal(null)),
      name: string.and(required),
      description: string.and(required)
    };
  }

  routes() {
    return {
      fetch: "/categories/{id}",
      save: "/categories",
      update: "/categories/{id}",
      delete: "/categories/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
