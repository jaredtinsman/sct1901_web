import BaseCollection from "../BaseCollection";
import Country from "./Country";

export default class Countries extends BaseCollection {
  model() {
    return Country;
  }

  defaults() {
    return {
      orderBy: "id"
    };
  }

  routes() {
    return {
      fetch: "countries"
    };
  }
}
