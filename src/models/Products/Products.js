import BaseCollection from "../BaseCollection";
import Product from "./Product";

export default class Products extends BaseCollection {
  model() {
    return Product;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "products"
    };
  }
}
