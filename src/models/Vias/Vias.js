import BaseCollection from "../BaseCollection";
import Via from "./Via";

export default class Vias extends BaseCollection {
  model() {
    return Via;
  }

  defaults() {
    return {
      orderBy: "name"
    };
  }

  routes() {
    return {
      fetch: "vias"
    };
  }
}
