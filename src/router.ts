import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Dashboard from "./views/admin/Dashboard.vue";
import ProductsMain from "./views/admin/Products/ProductsMain.vue";
import CategoriesMain from "./views/admin/Categories/CategoriesMain.vue";
import ViasMain from "./views/admin/Vias/ViasMain.vue";
import ProductDetails from "./views/shop/ProductDetails.vue"
import CountriesMain from "./views/admin/Countries/CountriesMain.vue";
import Inicio from "./views/Inicio.vue"
import Products from "./views/shop/Products.vue";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      children: [
        {
          path:"",
          name:"inicio",
          component: Inicio
        },
        {
          path:"products/:id/detail",
          name:"product_detail",
          component: ProductDetails
        },
        {
          path:"products",
          name:"products_shop",
          component: Products
        }
      ]
    },
    {
      path: "/admin",
      name: "admin",
      component: Dashboard,
      children: [
        {
          path: "products",
          name: "products",
          component: ProductsMain
        },
        {
          path: "categories",
          name: "categories",
          component: CategoriesMain
        },
        {
          path: "vias",
          name: "vias",
          component: ViasMain
        },
        {
          path: "countries",
          name: "countries",
          component: CountriesMain,
        }
      ]
    },
    {
      path: "/about",
      name: "about",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});
